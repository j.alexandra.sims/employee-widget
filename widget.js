
// Pulls request from employees.json file to display list of employees 
// that are currently in the office.
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
  if(xhr.readyState === 4) {
    // typeof operator looks at the item listed next 
    // (here, xhr.responseText and returns a word indicating what type of JavaScript element it is.
    //console.log(typeof xhr.responseText);
    
    // JSON.parse takes a string and tries to convert it to a JavaScript object.
    var employees = JSON.parse(xhr.responseText);
    // In JS, we get to the properties of an object using what's called dot syntax or dot notation.
    var statusHTML = '<ul class="bulleted">';
    for ( var i=0; i<employees.length; i += 1 ) {
      if ( employees[i].inoffice === true ) {
        statusHTML += '<li class="in">';
      }
      else {
        statusHTML += '<li class="out">';
      }
      statusHTML += employees[i].name;
      statusHTML += '</li>';
    }
    statusHTML += '</ul>';
    document.getElementById('employeeList').innerHTML = statusHTML;
  }
};
xhr.open('GET', 'data/employees.json');
xhr.send();


// Pulls request from rooms.json file to display list of available rooms 
// to reserve for meetings
var roomRequest = new XMLHttpRequest();
roomRequest.onreadystatechange = function () {
  if(roomRequest.readyState === 4 && roomRequest.status === 200) {
    var rooms = JSON.parse(roomRequest.responseText);
    var statusHTML = '<ul class="rooms">';
    for ( var i=0; i < rooms.length; i += 1 ) {
      if ( rooms[i].available === true ) {
        statusHTML += '<li class="empty">';
      }
      else {
        statusHTML += '<li class="full">';
      }
      statusHTML += rooms[i].room;
      statusHTML += '</li>';
    }
    statusHTML += '</ul>';
    document.getElementById('roomList').innerHTML = statusHTML;
  }
};
roomRequest.open('GET', '../data/rooms.json');
roomRequest.send();